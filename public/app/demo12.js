// connect person.js by 'api'
var app = angular.module('mainApp', ['ui.router', 'api', 'ui.bootstrap']);

app.controller('personCtrl', ['$scope', 'personService', function ($scope, personService) {
    $scope.persons = personService.persons;
}]);

app.controller('personDetailCtrl', ['$scope', '$stateParams', 'personService',
    function ($scope, $stateParams, personService) {
        var name = $stateParams.name;
        $scope.person = personService.persons.find(function(p) {
            return p.name == name;
        });
}]);

app.controller('tableCtrl', ['$scope', 'personService', function ($scope, personService) {
    $scope.persons = personService.persons;

    // sort
    $scope.orderByCol = "name";
    $scope.reverse = false;
    $scope.changeOrderByCol = function (colName) {
        $scope.orderByCol = colName;
        $scope.reverse = !$scope.reverse;
    }

    // search
    $scope.keyword = "";
    $scope.search = function() {
        var finded = [];
        $scope.persons.forEach(function (element, index) {
            if(element.name.includes($scope.keyword) || element.city.includes($scope.keyword)
                || element.country.includes($scope.keyword)) {
                finded.push({name: element.name, city: element.city, country: element.country})
            }
        })
        $scope.filteredItems = finded;
    };

    $scope.$watch("keyword", function (newVal, oldVal) {
        if(newVal != oldVal) {
            if(newVal == "")
                $scope.filteredItems = $scope.persons.slice(0, 3);
            else
                $scope.search();
        }
    })

    // pagenation
    $scope.curPage = 1;
    $scope.itemsPerPage = 3;
    //$scope.maxSize = 5;

    $scope.numOfPages = function () {
        return Math.ceil($scope.persons.length / $scope.itemsPerPage);
    };

    $scope.$watch('curPage + numPerPage', function() {
        var begin = (($scope.curPage - 1) * $scope.itemsPerPage);
        var end = begin + $scope.itemsPerPage;

        $scope.filteredItems = $scope.persons.slice(begin, end);
    });

}]);

app.config(['$stateProvider', function($stateProvider) {
    var homeState = {
        name: 'home',
        url: '/home',
        templateUrl: 'ui-template/home.html'
    };
    var personState = {
        name: 'person',
        url: '/person',
        templateUrl: 'ui-template/person.html',
        controller: 'personCtrl'
    };
    var personDetailState = {
        name: 'person.detail',
        url: '/:name',
        templateUrl: 'ui-template/person-detail.html',
        controller: 'personDetailCtrl'
    };
    var tableState = {
        name: 'table',
        url: '/table',
        templateUrl: 'ui-template/table.html',
        controller: 'tableCtrl'
    };

    // make it able to be clicked
    $stateProvider.state(homeState)
        .state(personState)
        .state(personDetailState)
        .state(tableState)
}]);
