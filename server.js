var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var personController = require("./controllers/person");

app.use(express.static(__dirname + "/public"));  // middleware
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(function (req, res, next) {
    res.setHeader("Access-control-allow-origin", '*');
    res.setHeader("Access-control-allow-methods", 'GET POST');
    res.setHeader("Access-control-allow-headers", 'X-Requested-With, content-type, Authorization');
    next();
});

app.get("/persons", personController.getPersons);
app.get("/persons/:index", personController.getPerson);
app.post("/persons", personController.postPersons);

var router = express.Router();

// url with value (parameter :x, :y)
router.get("/cal/add/:x/:y", function (req, res) {
    var x = +req.params.x;
    var y = +req.params.y;
    setTimeout(function () {
        res.send(""+(x+y));
    }, 1000)
})

router.get("/cal/multiply/:x/:y", function (req, res) {
    var x = +req.params.x;
    var y = +req.params.y;
    setTimeout(function () {
        res.send(""+(x*y));
    }, 1000)
})

// pass by object (parameter: req)
router.post("/cal/add", function (req, res) {
    var x = +req.body.x;
    var y = +req.body.y;
    setTimeout(function () {
        res.send(""+(x+y));
    }, 1000)
})

router.post("/cal/multiply", function (req, res) {
    var x = +req.body.x;
    var y = +req.body.y;
    setTimeout(function () {
        res.send(""+(x*y));
    }, 1000)
})



app.use("/", router);

app.listen(3000);

console.log("server started!");
